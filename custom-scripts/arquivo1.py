import SimpleHTTPServer
import SocketServer
import os.path, sys
import datetime
import platform
from datetime import timedelta
import cpuinfo
import os

with open('/proc/uptime', 'r') as f:
     uptime_seconds = float(f.readline().split()[0])
     uptime_string = str(timedelta(seconds = uptime_seconds))

#Formating string. The %s will be substituted by system data.
html = "<!DOCTYPE html>  \
		<html> \
			<body> \
				<h1>System Information</h1> \
				<p>Data e hora do sistema: %s </p> \
				<p>Uptime em segundos: %s </p> \
				<p>Modelo e velocidade do processador: %s</p> \
				<p>Capacidade processador usada: %s</p> \
			</body> \
		</html>"

#Handle de http requests.
class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	#Build the html file for every connection.	

	def makeindex(self):
		with open('/proc/uptime', 'r') as f1:
			uptime_seconds = float(f1.readline().split()[0])
			uptime_string = str(timedelta(seconds = uptime_seconds))
                CPU_Pct=str(round(float(os.popen('''grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage }' ''').readline()),2))
		tofile = html % (datetime.datetime.now(), str(uptime_seconds), cpuinfo.cpu.info[0]['model name'], CPU_Pct)
		f = open("index.html", "w")
		f.write(tofile)
		f.close()
		return
	
	#Method http GET.	
	def do_GET(self):
		self.makeindex()
		self.path = '/index.html'
		return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

#Start the webserver.
Handler = MyRequestHandler
server = SocketServer.TCPServer(('127.0.0.1', 8080), Handler)
server.serve_forever()
