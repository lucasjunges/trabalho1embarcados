import SimpleHTTPServer
import SocketServer
import os.path, sys
import datetime
import platform
from datetime import timedelta
import cpuinfo
import os
import psutil

with open('/proc/uptime', 'r') as f:
     uptime_seconds = float(f.readline().split()[0])
     uptime_string = str(timedelta(seconds = uptime_seconds))

#Formating string. The %s will be substituted by system data.
html = "<!DOCTYPE html>  \
		<html> \
			<body> \
				<h1>Informacao do sistema</h1> \
				<p>Data e hora do sistema: %s </p> \
				<p>Uptime em segundos: %s </p> \
				<p>Modelo e velocidade do processador: %s</p> \
				<p>Capacidade processador usada (percentual): %s</p> \
				<p>RAM total: %s MB</p> \
				<p>RAM usada: %s MB</p> \
				<p>Versao sistema operacional: %s</p> \
				<p>---------------------------------------------</p> \
				<h1>Processos em execucao</h1> "


#Handle de http requests.
class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	#Build the html file for every connection.	

	def makeindex(self):
		with open('/proc/uptime', 'r') as f1:
			uptime_seconds = float(f1.readline().split()[0])
			uptime_string = str(timedelta(seconds = uptime_seconds))
                CPU_Pct=str(round(float(os.popen('''grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage }' ''').readline()),2))
                CPU_Pct = psutil.cpu_percent()
		RAM_total = psutil.virtual_memory()[0]/1000
		RAM_usada = psutil.virtual_memory()[3]/1000
		tofile = html % (datetime.datetime.now(), str(uptime_seconds), cpuinfo.cpu.info[0]['model name'], CPU_Pct, RAM_total, RAM_usada, platform.platform())

		for i in psutil.pids():
                    tofile =  tofile + str(psutil.Process(i).pid) + "                    |                    " + str(psutil.Process(i).name()) + "<br>"
		tofile =  tofile + " </body> \
		</html>"
		f = open("index.html", "w") 
		f.write(tofile)
		f.close()
		return
	
	#Method http GET.	
	def do_GET(self):
		self.makeindex()
		self.path = '/index.html'
		return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

#Start the webserver.
Handler = MyRequestHandler
server = SocketServer.TCPServer(('192.168.1.10', 8080), Handler)
server.serve_forever()
