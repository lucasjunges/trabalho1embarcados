#!/bin/sh
set -e
chown -h -R 0:0 /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/target
/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/bin/makedevs -d /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/build/buildroot-fs/device_table.txt /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/target
   	rm -f /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/images/rootfs.ext2
	/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/sbin/mkfs.ext2 -d /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/target -r 1 -N 0 -m 5 -L "" -O ^64bit /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/images/rootfs.ext2 "60M" || { ret=$?; echo "*** Maybe you need to increase the filesystem size (BR2_TARGET_ROOTFS_EXT2_SIZE)" 1>&2; exit $ret; }
