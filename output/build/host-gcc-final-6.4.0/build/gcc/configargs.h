/* Generated automatically. */
static const char configuration_arguments[] = "./configure --prefix=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host --sysconfdir=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/etc --enable-static --target=i686-buildroot-linux-uclibc --with-sysroot=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot --disable-__cxa_atexit --with-gnu-ld --disable-libssp --disable-multilib --with-gmp=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host --with-mpc=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host --with-mpfr=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host --with-pkgversion='Buildroot 2018.02.4-ge18c8bb' --with-bugurl=http://bugs.buildroot.net/ --enable-libquadmath --disable-libsanitizer --enable-tls --disable-libmudflap --enable-threads --without-isl --without-cloog --disable-decimal-float --with-arch=pentiumpro --enable-languages=c --with-build-time-tools=/home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/bin --enable-shared --disable-libgomp";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "pentiumpro" } };
