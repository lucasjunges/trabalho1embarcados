cmd_archival/lzop.o := /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/bin/i686-buildroot-linux-uclibc-gcc -Wp,-MD,archival/.lzop.o.d   -std=gnu99 -Iinclude -Ilibbb  -include include/autoconf.h -D_GNU_SOURCE -DNDEBUG -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D"BB_VER=KBUILD_STR(1.27.2)" -DBB_BT=AUTOCONF_TIMESTAMP -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64  -Os  -march=i386 -mpreferred-stack-boundary=2 -Wall -Wshadow -Wwrite-strings -Wundef -Wstrict-prototypes -Wunused -Wunused-parameter -Wunused-function -Wunused-value -Wmissing-prototypes -Wmissing-declarations -Wno-format-security -Wdeclaration-after-statement -Wold-style-definition -fno-builtin-strlen -finline-limit=0 -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-guess-branch-probability -funsigned-char -static-libgcc -falign-functions=1 -falign-jumps=1 -falign-labels=1 -falign-loops=1 -fno-unwind-tables -fno-asynchronous-unwind-tables -fno-builtin-printf -Os     -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(lzop)"  -D"KBUILD_MODNAME=KBUILD_STR(lzop)" -c -o archival/lzop.o archival/lzop.c

deps_archival/lzop.o := \
  archival/lzop.c \
    $(wildcard include/config/lzop.h) \
    $(wildcard include/config/unlzop.h) \
    $(wildcard include/config/lzopcat.h) \
    $(wildcard include/config/lzop/compr/high.h) \
    $(wildcard include/config/desktop.h) \
  include/libbb.h \
    $(wildcard include/config/feature/shadowpasswds.h) \
    $(wildcard include/config/use/bb/shadow.h) \
    $(wildcard include/config/selinux.h) \
    $(wildcard include/config/feature/utmp.h) \
    $(wildcard include/config/locale/support.h) \
    $(wildcard include/config/use/bb/pwd/grp.h) \
    $(wildcard include/config/lfs.h) \
    $(wildcard include/config/feature/buffers/go/on/stack.h) \
    $(wildcard include/config/feature/buffers/go/in/bss.h) \
    $(wildcard include/config/feature/verbose.h) \
    $(wildcard include/config/feature/ipv6.h) \
    $(wildcard include/config/feature/seamless/xz.h) \
    $(wildcard include/config/feature/seamless/lzma.h) \
    $(wildcard include/config/feature/seamless/bz2.h) \
    $(wildcard include/config/feature/seamless/gz.h) \
    $(wildcard include/config/feature/seamless/z.h) \
    $(wildcard include/config/feature/check/names.h) \
    $(wildcard include/config/feature/prefer/applets.h) \
    $(wildcard include/config/long/opts.h) \
    $(wildcard include/config/feature/getopt/long.h) \
    $(wildcard include/config/feature/pidfile.h) \
    $(wildcard include/config/feature/syslog.h) \
    $(wildcard include/config/feature/individual.h) \
    $(wildcard include/config/echo.h) \
    $(wildcard include/config/printf.h) \
    $(wildcard include/config/test.h) \
    $(wildcard include/config/test1.h) \
    $(wildcard include/config/test2.h) \
    $(wildcard include/config/kill.h) \
    $(wildcard include/config/killall.h) \
    $(wildcard include/config/killall5.h) \
    $(wildcard include/config/chown.h) \
    $(wildcard include/config/ls.h) \
    $(wildcard include/config/xxx.h) \
    $(wildcard include/config/route.h) \
    $(wildcard include/config/feature/hwib.h) \
    $(wildcard include/config/feature/crond/d.h) \
    $(wildcard include/config/feature/securetty.h) \
    $(wildcard include/config/pam.h) \
    $(wildcard include/config/use/bb/crypt.h) \
    $(wildcard include/config/feature/adduser/to/group.h) \
    $(wildcard include/config/feature/del/user/from/group.h) \
    $(wildcard include/config/ioctl/hex2str/error.h) \
    $(wildcard include/config/feature/editing.h) \
    $(wildcard include/config/feature/editing/history.h) \
    $(wildcard include/config/feature/editing/savehistory.h) \
    $(wildcard include/config/feature/tab/completion.h) \
    $(wildcard include/config/feature/username/completion.h) \
    $(wildcard include/config/feature/editing/vi.h) \
    $(wildcard include/config/feature/editing/save/on/exit.h) \
    $(wildcard include/config/pmap.h) \
    $(wildcard include/config/feature/show/threads.h) \
    $(wildcard include/config/feature/ps/additional/columns.h) \
    $(wildcard include/config/feature/topmem.h) \
    $(wildcard include/config/feature/top/smp/process.h) \
    $(wildcard include/config/pgrep.h) \
    $(wildcard include/config/pkill.h) \
    $(wildcard include/config/pidof.h) \
    $(wildcard include/config/sestatus.h) \
    $(wildcard include/config/unicode/support.h) \
    $(wildcard include/config/feature/mtab/support.h) \
    $(wildcard include/config/feature/clean/up.h) \
    $(wildcard include/config/feature/devfs.h) \
  include/platform.h \
    $(wildcard include/config/werror.h) \
    $(wildcard include/config/big/endian.h) \
    $(wildcard include/config/little/endian.h) \
    $(wildcard include/config/nommu.h) \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include-fixed/limits.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include-fixed/syslimits.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/limits.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/features.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_config.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/cdefs.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/posix1_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/local_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/limits.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_local_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/posix2_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/xopen_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/stdio_lim.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/byteswap.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/byteswap.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/byteswap-common.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/wordsize.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/typesizes.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/byteswap-16.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/endian.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/endian.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include/stdint.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/stdint.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/wchar.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include/stdbool.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/unistd.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/posix_opt.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_posix_opt.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/environments.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include/stddef.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/confname.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/getopt.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/ctype.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_touplow.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/dirent.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/dirent.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/errno.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/errno.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/errno.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/errno.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/errno.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/errno-base.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/fcntl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/fcntl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/time.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/select.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/select.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sigset.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/time.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/sysmacros.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/pthreadtypes.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uio.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/stat.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/stat.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/inttypes.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/netdb.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/netinet/in.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/socket.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/uio.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/socket.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/socket_type.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sockaddr.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/socket.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/socket.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/sockios.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/sockios.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/in.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/siginfo.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/netdb.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/setjmp.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/setjmp.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/signal.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/signum.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sigaction.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sigcontext.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/sigcontext.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/int-ll64.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/bitsperlong.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/posix_types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/stddef.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/posix_types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/posix_types_32.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/posix_types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sigstack.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/ucontext.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/sigthread.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/paths.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/stdio.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_stdio.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/wchar.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/lib/gcc/i686-buildroot-linux-uclibc/6.4.0/include/stdarg.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/stdlib.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/waitflags.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/waitstatus.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/alloca.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/string.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/libgen.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/poll.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/poll.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/poll.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/ioctl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/ioctls.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/ioctls.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/ioctls.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/ioctl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/ioctl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/ioctl.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/ioctl-types.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/ttydefaults.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/mman.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/mman.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/mman-common.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/time.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/wait.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/termios.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/termios.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/uClibc_clk_tck.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/param.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/linux/param.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm/param.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/asm-generic/param.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/pwd.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/grp.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/shadow.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/mntent.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/sys/statfs.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/statfs.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/utmpx.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/bits/utmpx.h \
  /home/lucas/Trabalho1Embarcados/linuxdistro/buildroot/output/host/i686-buildroot-linux-uclibc/sysroot/usr/include/arpa/inet.h \
  include/xatonum.h \
  include/common_bufsiz.h \
  include/bb_archive.h \
    $(wildcard include/config/feature/tar/uname/gname.h) \
    $(wildcard include/config/feature/tar/long/options.h) \
    $(wildcard include/config/tar.h) \
    $(wildcard include/config/dpkg.h) \
    $(wildcard include/config/dpkg/deb.h) \
    $(wildcard include/config/feature/tar/gnu/extensions.h) \
    $(wildcard include/config/feature/tar/to/command.h) \
    $(wildcard include/config/feature/tar/selinux.h) \
    $(wildcard include/config/cpio.h) \
    $(wildcard include/config/rpm2cpio.h) \
    $(wildcard include/config/rpm.h) \
    $(wildcard include/config/feature/ar/create.h) \
  include/liblzo_interface.h \

archival/lzop.o: $(deps_archival/lzop.o)

$(deps_archival/lzop.o):
